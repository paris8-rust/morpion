Un Morpion

Fonctionnalités :

- une grille 3x3
- les symboles sont X et O

Au début l'utilisateur aura le choix entre :

- Joueur contre Joueur
- Joueur contre AI
- AI contre AI

Lorsque c'est le tour d'un joueur, il choisit le numéro de la case où il doit jouer.

Mamadou et Sterley
