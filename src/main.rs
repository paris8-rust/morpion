use std::io::{stdin,stdout,Write};
use rand::Rng;


// Fonction qui permet au joueur ou a l'ordinateur de gagner
fn gagner (touch: &mut [&str; 9]) -> (bool, u32) {
    let mut bool_val = true;

    if touch[0] == "X" && touch[3] == "X" && touch[6] == "X" ||
    touch[1] == "X" && touch[4] == "X" && touch[7] == "X" ||
    touch[2] == "X" && touch[5] == "X" && touch[8] == "X" ||
    touch[0] == "X" && touch[1] == "X" && touch[2] == "X" ||
    touch[3] == "X" && touch[4] == "X" && touch[5] == "X" ||
    touch[6] == "X" && touch[7] == "X" && touch[8] == "X" ||
    touch[0] == "X" && touch[4] == "X" && touch[8] == "X" ||
    touch[2] == "X" && touch[4] == "X" && touch[6] == "X"
    {
        (true, 1)
    } else if touch[0] == "O" && touch[3] == "O" && touch[6] == "O" ||
    touch[1] == "O" && touch[4] == "O" && touch[7] == "O" ||
    touch[2] == "O" && touch[5] == "O" && touch[8] == "O" ||
    touch[0] == "O" && touch[1] == "O" && touch[2] == "O" ||
    touch[3] == "O" && touch[4] == "O" && touch[5] == "O" ||
    touch[6] == "O" && touch[7] == "O" && touch[8] == "O" ||
    touch[0] == "O" && touch[4] == "O" && touch[8] == "O" ||
    touch[2] == "O" && touch[4] == "O" && touch[6] == "O"
    {
        (true, 2)
    } else { 
        for i in 0..touch.len() {
            if touch[i] != "X" && touch[i] != "O" {
                bool_val = false;

            }
        }
        if bool_val {
            (true, 0)            
        } else {
            (false, 0)
        }
    }
}
// Fonction qui permet au joueur ou a l'ordinateur de gagner

fn main () {

    println!("--------------------");
    println!("JEU DU MORPION");
    println!("--------------------");

    let mut joueur : u32;

    let mut plateau = ["1", "2", "3","4", "5", "6", "7", "8", "9"];

    let mut plateau2 = ["1", "2", "3","4", "5", "6", "7", "8", "9"];

    let mut plateau3 = ["1", "2", "3","4", "5", "6", "7", "8", "9"];

        // Configuration
        let mut m=String::new();
        print!("1 - Joueur Contre Joueur \n");
        print!("2 - Ordinateur Contre Ordinateur \n");
        print!("3 - Joueur Contre Ordinateur \n");
        print!("CHOISSISSEZ UNE CONFIGURATION : ");
        let _=stdout().flush();
        stdin().read_line(&mut m).expect("Did not enter a correct string");
        if let Some('\n')=m.chars().next_back() {
            m.pop();
        }
        if let Some('\r')=m.chars().next_back() {
            m.pop();
        }

        if m == "1" {
            println!("--------------------");
            println!("JOUEUR CONTRE JOUEUR");
            println!("--------------------");
            println!("Joueur 1 : X");
            println!("Joueur 2 : O");
            println!("--------------------");
            morpion(&mut plateau);
            while !gagner(&mut plateau).0 {
                joueur = 1;
                jeu(&mut plateau, joueur);
        
                if !gagner(&mut plateau).0 {
                    joueur = 2;
                    jeu(&mut plateau, joueur);
                }
            }
        
            if gagner (&mut plateau).1 == 1 {
                println!("Le Joueur 1 a gagné");
            } else if gagner (&mut plateau).1 == 2 {
                println!("Le Joueur 2 a gagné");
            } else {
                println!("EGALITE")
            }

        } else if m == "2" {
            println!("----------------------------");
            println!("ORDINATEUR CONTRE ORDINATEUR");
            println!("----------------------------");
            println!("Ordinateur 1 : X");
            println!("Ordinateur 2 : O");
            println!("--------------------");
            morpion2(&mut plateau2);
            while !gagner(&mut plateau2).0 {
                joueur = 1;
                jeu2(&mut plateau2, joueur);
        
                if !gagner(&mut plateau2).0 {
                    joueur = 2;
                    jeu2(&mut plateau2, joueur);
                }
            }
            if gagner (&mut plateau2).1 == 1 {
                println!("L'Ordinateur 1 a gagné");
            } else if gagner (&mut plateau2).1 == 2 {
                println!("L'Ordinateur 2 a gagné");
            } else {
                println!("EGALITE")
            }

        } else if m == "3" {
            println!("------------------------");
            println!("JOUEUR CONTRE ORDINATEUR");
            println!("------------------------");
            println!("Joueur : X");
            println!("Ordinateur : O");
            println!("--------------------");
            morpion3(&mut plateau3);
            while !gagner(&mut plateau3).0 {
                joueur = 1;
                jeu3(&mut plateau3, joueur);
        
                if !gagner(&mut plateau3).0 {
                    joueur = 2;
                    jeu3(&mut plateau3, joueur);
                }
            }
            if gagner (&mut plateau3).1 == 1 {
                println!("Le Joueur a gagné");
            } else if gagner (&mut plateau3).1 == 2 {
                println!("L'Ordinateur a gagné");
            } else {
                println!("EGALITE")
            }
        } else {
            println!("MAUVAISE COMMANDES !!!");
        }
        // Configuration
}

// Fonction pour la création du plateau de jeu2
fn morpion2 (touch: &mut [&str; 9]) {
    println!("[{}] [{}] [{}]", touch[0], touch[1], touch[2]);
    println!("[{}] [{}] [{}]", touch[3], touch[4], touch[5]);
    println!("[{}] [{}] [{}]", touch[6], touch[7], touch[8]);
}
// Fonction pour la création du plateau de jeu2

// Fonction permettant a deux ordinateur de jouer
fn jeu2 (plateau2: &mut [&str; 9], joueur: u32) {
        if joueur == 1 {
            print!("L'Ordinateur {} a joué : \n", joueur);
        } else {
            print!("L'Ordinateur {} a joué : \n", joueur);
        } 

        loop {
            let mut rng = rand::thread_rng();
            let com_rng = rng.gen_range(0, 9);

            let s = plateau2[com_rng];

            let mut multi: usize;
            multi = match s.trim().parse() {
                Ok(t) => t,
                Err(_) => continue, 
            };

            multi -= 1;
            if multi >= plateau2.len() {
                continue;

            } else if plateau2[multi].eq("X") || plateau2[multi].eq("O") {
                continue;
            
            } else {
                if joueur == 1 {
                    plateau2[multi] = "X";                
                } else {
                    plateau2[multi] = "O";
                }
                break;
            }
        }
    morpion2(plateau2);
}
// Fonction permettant a deux ordinateur de jouer

// Fonction pour la création du plateau de jeu
fn morpion (touch: &mut [&str; 9]) {
println!("[{}] [{}] [{}]", touch[0], touch[1], touch[2]);
println!("[{}] [{}] [{}]", touch[3], touch[4], touch[5]);
println!("[{}] [{}] [{}]", touch[6], touch[7], touch[8]);
}
// Fonction pour la création du plateau de jeu


// Fonction permettant a deux joueur de jouer
fn jeu (plateau: &mut [&str; 9], joueur: u32) {

        if joueur == 1 {
            print!("Le Joueur {} doit jouer : ", joueur);
        } else {
            print!("Le Joueur {} doit jouer : ", joueur);
        } 

        loop {
            let mut s = String::new();
            let _=stdout().flush();
            stdin().read_line(&mut s).expect("Did not enter a correct string");
            if let Some('\n')=s.chars().next_back() {
                s.pop();
            }
            if let Some('\r')=s.chars().next_back() {
                s.pop();
            }

            let mut multi: usize;
            multi = match s.trim().parse() {
                Ok(t) => t,
                Err(_) => continue, 
            };

            multi -= 1;
            if multi >= plateau.len() {
                continue;

            } else if plateau[multi].eq("X") || plateau[multi].eq("O") {
                println!("Case déjà occupée !!!");
                continue;
            
            } else {
                if joueur == 1 {
                    plateau[multi] = "X";                
                } else {
                    plateau[multi] = "O";
                }
                break;
            }
        }
    morpion(plateau);
}
// Fonction permettant a deux joueur de jouer


// Fonction pour la création du plateau de jeu3
fn morpion3 (touch: &mut [&str; 9]) {
    println!("[{}] [{}] [{}]", touch[0], touch[1], touch[2]);
    println!("[{}] [{}] [{}]", touch[3], touch[4], touch[5]);
    println!("[{}] [{}] [{}]", touch[6], touch[7], touch[8]);
    }
// Fonction pour la création du plateau de jeu3

// Fonction permettant a un joueur et un ordinateur de jouer
fn jeu3 (plateau3: &mut [&str; 9], joueur: u32) {
    let mut s2 = String::new();

    if joueur == 1 {
        print!("Le Joueur doit jouer : \n");
        let _=stdout().flush();
        stdin().read_line(&mut s2).expect("Did not enter a correct string");
        if let Some('\n')=s2.chars().next_back() {
            s2.pop();
        }
        if let Some('\r')=s2.chars().next_back() {
            s2.pop();
        }
    } else {
        print!("L'Ordinateur a joué : \n");
    }
    loop {
        let mut rng = rand::thread_rng();
        let com_rng = rng.gen_range(0, 9);

        let s = plateau3[com_rng];

        let mut multi: usize;
        if joueur == 1 {
            multi = match s2.trim().parse() {
                Ok(t) => t,
                Err(_) => continue, 
            };
        } else {
            multi = match s.trim().parse() {
                Ok(t) => t,
                Err(_) => continue, 
            };
        }
        multi -= 1;
        if multi >= plateau3.len() {
            continue;

        } else if plateau3[multi].eq("X") || plateau3[multi].eq("O") {
            continue;
            
        } else {
            if joueur == 1 {
                plateau3[multi] = "X";               
            } else {
                plateau3[multi] = "O";
            }
            break;
        }
    }
    morpion3(plateau3);
}
// Fonction permettant a un joueur et un ordinateur de jouer



